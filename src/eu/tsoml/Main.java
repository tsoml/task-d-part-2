package eu.tsoml;

import java.util.Scanner;

public class Main {

    enum Currency {USD, EUR, GBR}

    public static void main(String[] args) {


        Bank monoBank = new Bank("MonoBank", 28.41, 30.5, 34.77);
        Bank privatBank = new Bank("PrivatBank", 28.31, 30.3, 34.27);
        Bank universalBank = new Bank("Universal Bank", 28.21, 30.1, 34.37);

        Scanner scan = new Scanner(System.in);

        System.out.println("Enter bank name: ");
        String bank = scan.nextLine();

        System.out.println("Enter the amount of UAH that you want to change: ");
        int amount = Integer.parseInt(scan.nextLine());

        System.out.println("Enter the currency to convert (USD,EUR or GBR): ");
        String currency = scan.nextLine();


        if (bank.equalsIgnoreCase(monoBank.name)) {
            convert(monoBank, amount, currency);
        } else if (bank.equalsIgnoreCase(privatBank.name)) {
            convert(privatBank, amount, currency);
        } else if (bank.equalsIgnoreCase(universalBank.name)) {
            convert(universalBank, amount, currency);
        } else System.out.println("No access to the " + bank);

    }

    public static void convert(Bank bank, double amount, String currency) {

        switch (Currency.valueOf(currency)) {

            case USD:
                System.out.println(String.format("You money in %s: %.2f", Currency.USD, amount / bank.COURSE_USD));
                break;

            case EUR:
                System.out.println(String.format("You money in %s: %.2f", Currency.EUR, amount / bank.COURSE_EUR));
                break;

            case GBR:
                System.out.println(String.format("You money in %s: %.2f", Currency.GBR, amount / bank.COURSE_GBR));
                break;

            default:
                System.err.println("Can't convert to " + currency);
                break;
        }

    }
}