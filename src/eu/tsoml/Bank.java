package eu.tsoml;

public class Bank {

    String name;
    double COURSE_USD;
    double COURSE_EUR;
    double COURSE_GBR;

    public Bank(String name, double COURSE_USD, double COURSE_EUR, double COURSE_GBR) {
        this.name = name;
        this.COURSE_USD = COURSE_USD;
        this.COURSE_EUR = COURSE_EUR;
        this.COURSE_GBR = COURSE_GBR;
    }
}
